package ija.internal;

import ija.internal.utils.CollectionUtils;
import ija.model.Block;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class WaveIteratorTest {

    private List<Set<Block>> prepareComplex() {
        Manager manager = new Manager(CollectionUtils.set());
        List<Block> wave1 = new ArrayList<>(TestUtils.generate(3));
        List<Block> wave2 = new ArrayList<>(TestUtils.generate(4));
        List<Block> wave3 = new ArrayList<>(TestUtils.generate(2));

        manager.port(wave1.get(0), wave2.get(1));
        manager.port(wave1.get(1), wave2.get(0));
        manager.port(wave1.get(1), wave2.get(3));
        manager.port(wave1.get(2), wave2.get(2));

        manager.port(wave2.get(0), wave3.get(0));
        manager.port(wave2.get(1), wave3.get(1));
        manager.port(wave2.get(2), wave3.get(0));
        manager.port(wave2.get(3), wave3.get(1));


        return CollectionUtils.list(
                new HashSet<>(wave1),
                new HashSet<>(wave2),
                new HashSet<>(wave3)
        );
    }

    private List<Set<Block>> prepareSimple() {
        Manager manager = new Manager(CollectionUtils.set());
        List<Block> wave1 = new ArrayList<>(TestUtils.generate(2));
        List<Block> wave2 = new ArrayList<>(TestUtils.generate(2));

        manager.port(wave1.get(0), wave2.get(0));
        manager.port(wave1.get(1), wave2.get(1));


        return CollectionUtils.list(
                new HashSet<>(wave1),
                new HashSet<>(wave2)
        );
    }

    @Test
    public void wavingSimpleCase() {
        List<Set<Block>> prepare = prepareSimple();

        WaveIterator iterator = new WaveIterator(prepare.get(0));

        for (int i = 0; iterator.hasNext(); ++i) {
            Collection<Block> next = iterator.next();
            assertEquals(prepare.get(i), next);
        }
    }

    @Test
    public void wavingComplexCase() {
        List<Set<Block>> prepare = prepareComplex();

        WaveIterator iterator = new WaveIterator(prepare.get(0));

        for (int i = 0; iterator.hasNext(); ++i) {
            Collection<Block> next = iterator.next();
            assertEquals(prepare.get(i), next);
        }
    }
}