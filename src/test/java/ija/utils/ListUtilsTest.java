package ija.utils;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

public class ListUtilsTest {
    private List<String> generate(Integer number) {
        return IntStream.range(1, number + 1)
                .mapToObj(it -> String.format("item%d", it))
                .collect(Collectors.toList());
    }


    @Test
    public void list() {
        assertTrue(CollectionUtils.list().isEmpty());
    }

    @Test
    public void listCopy() {
        List<String> items = generate(5);
        List<String> actual = CollectionUtils.list(items);
        assertEquals(items, actual);
        assertNotSame(items, actual);
    }

    @Test
    public void listOfItems() {
        List<String> items = generate(2);
        List<String> actual = CollectionUtils.list("item1", "item2");
        assertEquals(items, actual);
        assertNotSame(items, actual);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void immutable() {
        List<String> actual = CollectionUtils.immutableList();

        assertEquals(0, actual.size());
        actual.add("throw exception");
    }

    @Test(expected = UnsupportedOperationException.class)
    public void immutableCopy() {
        List<String> items = generate(2);

        List<String> actual = CollectionUtils.immutableList(items);
        assertEquals(items, actual);
        assertNotSame(items, actual);

        actual.add("throw exception");
    }

    @Test(expected = UnsupportedOperationException.class)
    public void immutableOfItems() {
        List<String> items = generate(3);

        List<String> actual = CollectionUtils.immutableList("item1", "item2", "item3");
        assertEquals(items, actual);
        assertNotSame(items, actual);

        actual.add("throw exception");
    }

    @Test
    public void testConsumer() {
        List<String> items = generate(3);
        String object = "object";

        AtomicBoolean bool = new AtomicBoolean(false);
        
        List<String> actual = CollectionUtils.consume(items, object, (list, item) -> {
            bool.set(true);
            assertNotSame(list, items);
            assertSame(object, item);
        });

        assertTrue(bool.get());
        assertEquals(items, actual);
        assertNotSame(items, actual);
    }
}
