package ija.utils;

import ija.events.Action;
import ija.events.BlockAction;
import ija.gui.State;
import ija.gui.State.ActiveScheme;
import ija.gui.components.app.App;
import ija.gui.components.model.BlockComp;
import ija.gui.components.model.PortComp;
import ija.model.Block;
import ija.model.Port;

public class StateUtils extends AbstractUtils {

    public static BlockComp getBlockComponent(State state, Block block) {
        return getBlockComponent(state.getActiveScheme(), block);
    }

    public static PortComp getPortComponent(State state, Port port) {
        return getPortComponent(state.getActiveScheme(), port);
    }

    public static BlockComp getBlockComponent(ActiveScheme scheme, Block block) {
        return scheme.getBlocks().get(block);
    }

    public static PortComp getPortComponent(ActiveScheme scheme, Port port) {
        return scheme.getPorts().get(port);
    }

    public static void distribute(State state, Action action) {
        if (action instanceof BlockAction)
            getBlockComponent(state, ((BlockAction) action).getBlock()).consume(state, action);
        else throw App.unknownAction(action);
    }
}
