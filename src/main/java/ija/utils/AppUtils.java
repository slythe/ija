package ija.utils;

import ija.gui.components.model.PortComp;
import ija.model.Block;
import ija.model.Scheme;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AppUtils extends AbstractUtils {
    private static int i = 0;

    public static Scheme mockScheme() {
        Scheme scheme = new Scheme("Scheme " + ++i);
        scheme.setAll(Stream.generate(Block::new).limit(5).collect(Collectors.toSet()));
        return scheme;
    }

    public static boolean isValid(PortComp component) {
        return component.getTarget() != null || component.getSource() != null;
    }
}
