package ija.utils;

/**
 * Abstract class representation of utils.
 *
 * @author Vojtěch Bargl (xbargl01@stud.fit.vutbr.cz)
 */
abstract class AbstractUtils {
    AbstractUtils() { throw new IllegalStateException(); }
}
