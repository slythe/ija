package ija.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;

/**
 * Utils for collections.
 *
 * @author Vojtěch Bargl (xbargl01@stud.fit.vutbr.cz)
 */
public final class CollectionUtils extends AbstractUtils {

    //region List
    /** Creates simple list of given items. */
    @SafeVarargs
    public static <T> List<T> list(T... other) {
        List<T> objects = new ArrayList<>();
        Collections.addAll(objects, other);
        return objects;
    }

    /** Duplicates given list. */
    public static <T> List<T> list(Collection<T> other) {
        return new ArrayList<>(other);
    }

    /** Creates immutable list from given items. */
    @SafeVarargs
    public static <T> List<T> immutableList(T... others) {
        return Collections.unmodifiableList(list(others));
    }

    /** Creates immutable list from given list. */
    public static <T> List<T> immutableList(List<T> other) {
        return Collections.unmodifiableList(other);
    }


    /** Executes list action on duplicated list when specific item. */
    public static <T> List<T> consume(List<T> list, T object, BiConsumer<List<T>, T> consumer) {
        List<T> other = new ArrayList<>(list);
        consumer.accept(other, object);
        return other;
    }
    //endregion

    //region Set
    /** Duplicates given set. */
    public static <T> Set<T> set(Collection<T> other) {
        return new HashSet<>(other);
    }

    /** Creates simple set of given items. */
    @SafeVarargs
    public static <T> Set<T> set(T... other) {
        Set<T> objects = new HashSet<>();
        Collections.addAll(objects, other);
        return objects;
    }

    /** Create immutable set of given set. */
    public static <T> Set<T> immutableSet(Set<T> other) {
        return Collections.unmodifiableSet(other);
    }

    /** Create immutable set of given set. */
    public static <T> Set<T> immutableSet(T... other) {
        return Collections.unmodifiableSet(set(other));
    }

    public static <K, V> Map<K, V> map() {
        return new HashMap<>();
    }
    //endregion
}
