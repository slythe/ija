package ija;

import redux.api.Reducer;
import redux.api.Store;

public class SimpleStore<S> implements Store<S> {
    private S state;
    private Reducer<S> reducer;

    public SimpleStore(S state) {
        this.state = state;
    }

    @Override
    public S getState() {
        return state;
    }

    @Override
    public Subscription subscribe(Subscriber subscriber) {
        throw new IllegalStateException("Not implemented!");
    }

    @Override
    public void replaceReducer(Reducer<S> reducer) {
        this.reducer = reducer;

    }

    @Override
    public Object dispatch(Object o) {
        state = reducer.reduce(state, o);
        return o;
    }
}
