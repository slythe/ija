package ija.gui.features;

import javafx.beans.property.SimpleBooleanProperty;

public abstract class AbstractFeature implements Feature {
    private boolean installed = false;


    protected void validateAndLock() {
        if (installed) throw new IllegalStateException("Already installed!");
        installed = true;
    }

    protected final void validate(Boolean condition) {
        if (!condition) throw new IllegalArgumentException();
    }

    protected final void check() {
        if (installed) throw new IllegalStateException("Cannot be altered, already installed!");
    }
}
