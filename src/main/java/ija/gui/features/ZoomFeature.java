package ija.gui.features;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.function.Predicate;
import javafx.beans.binding.Bindings;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.ScrollEvent;
import javafx.scene.transform.Scale;

public class ZoomFeature extends AbstractFeature {

    private Scale scale = new Scale(1, 1);

    private Predicate<ScrollEvent> when;
    private Node applicable;
    private Set<Node> observedOn = new HashSet<>();

    //region Setup
    public ZoomFeature when(Predicate<ScrollEvent> when) {
        this.when = when;
        return this;
    }

    public ZoomFeature applyTo(Node applicable) {
        this.applicable = applicable;
        return this;
    }

    public ZoomFeature observeOn(Node... focusable) {
        Collections.addAll(observedOn, focusable);
        return this;
    }
    //endregion

    //region Handlers
    private final EventHandler<ScrollEvent> scroll = it -> {
        if (!when.test(it)) return;

        double zoomFactor = 0.05;
        double deltaY = it.getDeltaY();

        zoomFactor = deltaY > 0 ? +zoomFactor : -zoomFactor;

        scale.setX(scale.getX() + zoomFactor);
        scale.setY(scale.getY() + zoomFactor);
    };
    //endregion

    @Override
    protected void validateAndLock() {
        super.validateAndLock();

        Objects.requireNonNull(when);
        Objects.requireNonNull(applicable);
        validate(!observedOn.isEmpty());
    }

    @Override
    public Featured install() {
        validateAndLock();

        applicable.getTransforms().add(scale);

        // TODO: buggy
        scale.pivotXProperty().bind(Bindings.createDoubleBinding(() -> applicable.getLayoutX() + applicable.getLayoutBounds().getWidth() / 2, applicable.layoutXProperty(), applicable.layoutBoundsProperty()));
        scale.pivotYProperty().bind(Bindings.createDoubleBinding(() -> applicable.getLayoutY() + applicable.getLayoutBounds().getHeight() / 2, applicable.layoutYProperty(), applicable.layoutBoundsProperty()));

        observedOn.forEach(focusedOn -> focusedOn.addEventHandler(ScrollEvent.SCROLL, scroll));

        return this::uninstall;
    }

    private void uninstall() {
        applicable.getTransforms().remove(scale);

        scale.pivotXProperty().unbind();
        scale.pivotYProperty().unbind();

        observedOn.forEach(focusedOn -> focusedOn.addEventHandler(ScrollEvent.SCROLL, scroll));
    }

    public static ZoomFeature create() {
        return new ZoomFeature();
    }

}