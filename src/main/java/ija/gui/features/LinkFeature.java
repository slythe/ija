package ija.gui.features;

import ija.utils.CollectionUtils;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.Predicate;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;
import javafx.scene.input.DragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;

import static java.util.stream.Collectors.toMap;

public class LinkFeature extends AbstractFeature {

    private static final DataFormat DEFAULT = new DataFormat("#link-feature");

    private Predicate<MouseEvent> when;
    private Node applicable;
    private Map<DataFormat, Object> data = new HashMap<>();
    private Consumer<MouseEvent> detected;

    //region Setup
    public LinkFeature when(Predicate<MouseEvent> when) {
        check();
        this.when = when;
        return this;
    }
    public LinkFeature detected(Consumer<MouseEvent> detected) {
        check();
        this.detected = detected;
        return this;
    }

    public LinkFeature applyTo(Node node) {
        check();
        this.applicable = node;
        return this;
    }

    public LinkFeature with(DataFormat name, Object data) {
        check();
        this.data.put(name, data);
        return this;
    }
    //endregion

    //region Handlers
    private final EventHandler<MouseEvent> dragDetected = it -> {
        if (!when.test(it)) return;

        if (data.isEmpty()) data.put(DEFAULT, UUID.randomUUID());
        applicable.startDragAndDrop(TransferMode.LINK).setContent(data);

        if (detected != null) detected.accept(it);

        it.consume();
    };
    //endregion

    @Override
    protected void validateAndLock() {
        super.validateAndLock();

        Objects.requireNonNull(when);
        Objects.requireNonNull(applicable);
    }

    @Override
    public Featured install() {
        validateAndLock();

        applicable.addEventHandler(MouseEvent.DRAG_DETECTED, dragDetected);

        return this::uninstall;
    }

    private void uninstall() {
        applicable.removeEventHandler(MouseEvent.DRAG_DETECTED, dragDetected);
    }

    public static LinkFeature create() {
        return new LinkFeature();
    }
}
