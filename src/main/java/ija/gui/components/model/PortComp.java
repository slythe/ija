package ija.gui.components.model;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import ija.Container;
import ija.events.ContentAction.RemovePort;
import ija.gui.MenuItemBuilder;
import ija.gui.State;
import ija.gui.components.ModelComponent;
import ija.gui.components.extra.Link;
import ija.model.DomainEntry;
import ija.model.Port;
import ija.utils.CollectionUtils;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;
import javafx.fxml.FXML;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Tooltip;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.DataFormat;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import redux.api.Store;

import static com.fasterxml.jackson.core.JsonGenerator.Feature.QUOTE_FIELD_NAMES;
import static com.fasterxml.jackson.core.JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES;

public class PortComp extends ModelComponent<Port, VBox> {
    private static final String FXML_VIEW = "fxml/model/port.fxml";

    public static final DataFormat MOVING = new DataFormat("moving-port");
    public static final DataFormat LINKING = new DataFormat("linking-from-port");

    private final Store<State> store;

    @FXML
    private Circle circle;
    @FXML
    private Label type;

    private Link source;
    private Link target;

    private ContextMenu menu = new ContextMenu();
    private Tooltip tooltip = new Tooltip();

    public PortComp(Store<State> store, Port port) {
        super(port, FXML_VIEW);
        this.store = store;
    }


    @Override
    public void onInit() {
        super.onInit();

        if (model.getSource() == null)
            menu.getItems().add(MenuItemBuilder.of("Upravit").action(this::edit).build());
        menu.getItems().add(MenuItemBuilder.of("Odstranit").action(this::remove).build());
    }

    private void edit() {
        TextInputDialog dialog = new TextInputDialog(serialize(model.toMap()));
        dialog.setGraphic(null);
        dialog.setTitle("Úprava portu");
        dialog.setHeaderText(null);
        dialog.setContentText("Hodnoty:");
        Optional<String> result = dialog.showAndWait();
        result.ifPresent(s -> model.fromMap(deserialize(s)));
    }

    public void remove() {
        Stream<Link> stream = CollectionUtils.list(source, target).stream().filter(Objects::nonNull);
        source = null;
        target = null;
        stream.forEach(Link::remove);

        store.dispatch(new RemovePort(model));
    }

    @FXML
    private void onMenuRequested(ContextMenuEvent it) {
        menu.show(getView(), it.getScreenX(), it.getScreenY());
        it.consume();
    }

    public void setSource(Link source) {
        this.source = source;
    }

    public Link getSource() {
        return source;
    }

    public void setTarget(Link target) {
        this.target = target;
    }

    public Link getTarget() {
        return target;
    }



    private String serialize(Map<String, Double> type) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(QUOTE_FIELD_NAMES, false);
            return mapper.writeValueAsString(type);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Map<String, Double> deserialize(String content) {
        TypeReference typeRef = new TypeReference<HashMap<String,Double>>() {};
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(ALLOW_UNQUOTED_FIELD_NAMES, true);
            return mapper.readValue(content, typeRef);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
