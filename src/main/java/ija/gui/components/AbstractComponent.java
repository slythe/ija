package ija.gui.components;

import ija.utils.ComponentUtils;
import java.util.UUID;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableBooleanValue;
import javafx.beans.value.ObservableValue;

public abstract class AbstractComponent<V> implements Component<V> {

    protected final String id = UUID.randomUUID().toString();
    protected final V view;

    protected AbstractComponent(String fxml) {
        view = ComponentUtils.load(fxml, this);
    }

    @Override
    public void onInit() {
    }

    @Override
    public final V getView() {
        return view;
    }
}
