package ija.gui.components.extra;

import ija.gui.State;
import ija.gui.components.model.BlockComp;
import ija.gui.components.model.PortComp;
import ija.model.Block;
import ija.model.Port;
import ija.utils.CollectionUtils;
import java.util.List;
import javafx.scene.paint.Color;
import redux.api.Store;

public class SourceLink extends Link {

    private static final Color COLOR = Color.rgb(168, 168, 168);

    public SourceLink(Store<State> store, BlockComp block, PortComp port) {
        super(store, block, port);
        getView().setStroke(COLOR);
    }

    @Override
    public void remove() {
        getPort().setSource(null);
        super.remove();
    }

    @Override
    protected void linkTogether() {
        Block block = getBlock().getModel();
        Port port = getPort().getModel();
        port.setSource(block);
        block.setOutputs(CollectionUtils.consume(block.getOutputs(), port, List::add));

        getBlock().getLinks().add(this);
        getPort().setSource(this);
    }
}
