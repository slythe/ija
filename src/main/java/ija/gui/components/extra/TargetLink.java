package ija.gui.components.extra;

import ija.gui.State;
import ija.gui.components.model.BlockComp;
import ija.gui.components.model.PortComp;
import ija.model.Block;
import ija.model.Port;
import ija.utils.CollectionUtils;
import java.util.List;
import java.util.Optional;
import javafx.scene.paint.Color;
import redux.api.Store;

public class TargetLink extends Link {

    private static final Color COLOR = Color.rgb(87, 87, 87);

    public TargetLink(Store<State> store, BlockComp block, PortComp port) {
        super(store, block, port);
        getView().setStroke(COLOR);
    }

    @Override
    public void remove() {
        getPort().setTarget(null);
        super.remove();
    }

    @Override
    protected void linkTogether() {
        Block block = getBlock().getModel();
        Port port = getPort().getModel();
        port.setTarget(block);
        block.setInputs(CollectionUtils.consume(block.getInputs(), port, List::add));

        getBlock().getLinks().add(this);
        getPort().setTarget(this);
    }
}
