package ija.gui.components;

import java.io.Serializable;

public interface Component<V> {

    void onInit();

    V getView();
}
