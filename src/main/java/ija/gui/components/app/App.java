package ija.gui.components.app;

import ija.Container;
import ija.SimpleStore;
import ija.config.storage.Storage;
import ija.events.Action;
import ija.events.HierarchyAction.AddScheme;
import ija.events.HierarchyAction.ChangeScheme;
import ija.events.HierarchyAction.SaveScheme;
import ija.gui.State;
import ija.gui.State.ActiveScheme;
import ija.gui.components.AbstractComponent;
import javafx.fxml.FXML;
import javafx.scene.control.SplitPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.VBox;
import redux.api.Store;

public class App extends AbstractComponent<VBox> {
    private static final String FXML = "fxml/app/app.fxml";

    private Store<State> store = new SimpleStore<>(new State());
    private Storage storage;

    private Menu menu = new Menu(store);
    private Hierarchy hierarchy = new Hierarchy(store);
    private Content content = new Content(store);

    public App() {
        super(FXML);
        this.storage = Container.getInstance().get(Storage.class);
        store.replaceReducer(this::consumer);
    }

    @FXML
    private SplitPane splitPane;

    @Override
    public void onInit() {
        super.onInit();

        this.storage.load().forEach(it -> store.dispatch(new AddScheme(it)));
        if(! this.store.getState().getSchemes().isEmpty()) {
            store.dispatch(new ChangeScheme(0, store.getState().getSchemes().get(0)));
        }

        view.getChildren().add(0, menu.getView());
        splitPane.getItems().addAll(hierarchy.getView(), content.getView());
    }

    private State consumer(State state, Object action) {
        debugAction(action);


        if (action instanceof ChangeScheme)
            changeScheme(state, (ChangeScheme) action);

        if(action instanceof SaveScheme)
            this.saveScheme(state);


        hierarchy.consume(state, (Action) action);
        content.consume(state, (Action) action);

        debugState(state);

        return state;
    }

    private void debugAction(Object action) {
        System.out.println("Action: " + action.getClass());
        System.out.println();
    }

    private void debugState(State state) {
        System.out.println("State: [");
        System.out.println("\tActive scheme: " + (state.getActiveScheme() != null));
        if (state.getActiveScheme() != null) {
            System.out.println("\tScheme: " + state.getSchemes().get(state.getActiveScheme().getIndex()));
            System.out.println("\tActive:");
            System.out.println("\t\tIndex: " + state.getActiveScheme().getIndex());
            System.out.println("\t\tBlocks: " + state.getActiveScheme().getBlocks().size());
            System.out.println("\t\tPorts: " + state.getActiveScheme().getPorts().size());
        }
        System.out.println("\tSchemes: " + state.getSchemes());
        System.out.println("]");
        System.out.println("\n - - - - - - - - - - - - - - - - -\n");
    }

    //region Consumers
    private void changeScheme(State state, ChangeScheme action) {
        if(state.getActiveScheme() != null)
            this.saveScheme(state);

        ActiveScheme activeScheme = new ActiveScheme(action.index);
        state.setActiveScheme(activeScheme);

        action.scheme.getAll().forEach(block -> {
            activeScheme.getBlocks().put(block, null);
            block.getInputs().forEach(port -> activeScheme.getPorts().put(port, null));
            block.getOutputs().forEach(port -> activeScheme.getPorts().put(port, null));
        });
    }

    private void saveScheme(State state) {
        ActiveScheme active = state.getActiveScheme();
        state.getSchemes().get(active.getIndex()).setAll(active.getBlocks().keySet());

        this.storage.store(state.getSchemes());
    }

    private void saveApp(State state) {
        // TODO: save
    }
    //endregion


    public static boolean shouldMove(MouseEvent it) {
        return it.isPrimaryButtonDown() && !it.isAltDown() && !it.isControlDown();
    }

    public static boolean shouldMovingAlternatively(MouseEvent it) {
        return it.isPrimaryButtonDown() && it.isAltDown() && !it.isControlDown();
    }

    public static boolean shouldLink(MouseEvent it) {
        return it.isPrimaryButtonDown() && it.isControlDown() && !it.isAltDown();
    }

    public static boolean shouldZoom(ScrollEvent it) {
        return it.isControlDown();
    }

    public static IllegalStateException unknownAction(Object e) {
        return new IllegalStateException("Unknown action [" + e.getClass() + "]!");
    }
}
