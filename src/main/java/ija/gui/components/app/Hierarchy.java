package ija.gui.components.app;

import ija.events.Action;
import ija.events.AppAction;
import ija.events.HierarchyAction.*;
import ija.gui.State;
import ija.gui.components.AbstractComponent;
import ija.model.Scheme;
import javafx.fxml.FXML;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.util.converter.DefaultStringConverter;
import redux.api.Store;

public class Hierarchy extends AbstractComponent<ListView<String>> {

    private static final String FXML = "fxml/app/hierarchy.fxml";

    private Store<State> store;

    public Hierarchy(Store<State> store) {
        super(FXML);
        this.store = store;
    }

    @Override
    public void onInit() {
        super.onInit();

        view.setEditable(true);
        view.setCellFactory(this::factory);
        view.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
    }

    private ListCell<String> factory(ListView<String> view) {
        return new TextFieldListCell<String>(new DefaultStringConverter()) {
            @Override
            public void commitEdit(String newValue) {
                super.commitEdit(newValue);
                store.dispatch(new RenameScheme(getIndex(), newValue));
            }
        };
    }

    //region FXML handlers
    @FXML
    private void loadScheme() {
        int index = view.getSelectionModel().getSelectedIndex();
        if (index >= 0)
            store.dispatch(new ChangeScheme(index, store.getState().getSchemes().get(index)));
    }

    @FXML
    private void newScheme() {
        store.dispatch(new AddScheme(new Scheme("Nové schéma")));
    }

    @FXML
    private void deleteScheme() {
        store.dispatch(new RemoveScheme(view.getSelectionModel().getSelectedIndex()));
    }

    @FXML
    private void saveScheme() {
        int index = view.getSelectionModel().getSelectedIndex();
        if (index >= 0)
            store.dispatch(new SaveScheme());
    }
    //endregion

    //region Consumers
    public void consume(State state, Action action) {
        if (action == AppAction.LOAD) loadSchemes();
        else if (action instanceof ChangeScheme) changeScheme((ChangeScheme)action);
        else if (action instanceof AddScheme) addScheme(state, (AddScheme) action);
        else if (action instanceof RemoveScheme) removeScheme(state, (RemoveScheme) action);
        else if (action instanceof RenameScheme) updateScheme(state, (RenameScheme) action);
    }

    private void loadSchemes() {
        view.getItems().clear();
        store.getState().getSchemes().stream().map(Scheme::getName).forEach(view.getItems()::add);
    }

    private void changeScheme(ChangeScheme action) {
        view.getSelectionModel().select(action.index);
    }

    private void updateScheme(State state, RenameScheme action) {
        state.getSchemes().get(action.index).setName(action.name);
        view.getItems().set(action.index, action.name);
    }

    private void removeScheme(State state, RemoveScheme action) {
        int index = action.index;
        state.getSchemes().remove(index);
        view.getItems().remove(index);
    }

    private void addScheme(State state, AddScheme action) {
        state.getSchemes().add(action.scheme);
        view.getItems().add(action.scheme.getName());
    }
    //endregion
}
