package ija.gui.components.app;

import ija.gui.State;
import ija.gui.components.AbstractComponent;
import javafx.scene.control.MenuBar;
import redux.api.Store;

public class Menu extends AbstractComponent<MenuBar> {

    private static final String FXML = "fxml/app/menu.fxml";

    private final Store<State> state;

    public Menu(Store<State> state) {
        super(FXML);
        this.state = state;
    }
}
