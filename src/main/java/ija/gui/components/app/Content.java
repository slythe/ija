package ija.gui.components.app;

import ija.events.Action;
import ija.events.BlockAction;
import ija.events.ContentAction.AddBlock;
import ija.events.ContentAction.AddPort;
import ija.events.ContentAction.RemoveBlock;
import ija.events.HierarchyAction.ChangeScheme;
import ija.events.LinkAction;
import ija.events.LinkAction.LinkExistingAsSource;
import ija.events.LinkAction.LinkExistingAsTarget;
import ija.events.LinkAction.LinkBlocks;
import ija.events.LinkAction.RemoveLink;
import ija.events.ContentAction.RemovePort;
import ija.gui.MenuItemBuilder;
import ija.gui.State;
import ija.gui.State.ActiveScheme;
import ija.gui.components.AbstractComponent;
import ija.gui.components.Component;
import ija.gui.components.extra.Link;
import ija.gui.components.extra.SourceLink;
import ija.gui.components.extra.TargetLink;
import ija.gui.components.model.BlockComp;
import ija.gui.components.model.PortComp;
import ija.gui.features.LinkFeature;
import ija.gui.features.LinkableFeature;
import ija.gui.features.MoveFeature;
import ija.gui.features.PlatformMoveFeature;
import ija.model.Block;
import ija.model.Port;
import ija.utils.CollectionUtils;
import ija.utils.StateUtils;
import java.util.List;
import java.util.UUID;
import javafx.application.Platform;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.control.ContextMenu;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.DataFormat;
import javafx.scene.input.DragEvent;
import javafx.scene.layout.Pane;
import redux.api.Store;

import static ija.utils.ComponentUtils.canBeLinked;
import static ija.utils.ComponentUtils.comp;
import static ija.utils.ComponentUtils.noLinkBetween;
import static ija.utils.ComponentUtils.midpoint;
import static ija.utils.ComponentUtils.pos;
import static ija.utils.ComponentUtils.random;
import static ija.utils.ComponentUtils.translate;

public class Content extends AbstractComponent<Pane> {

    private static final String FXML = "fxml/app/content.fxml";

    private Store<State> store;

    @FXML
    private Group canvas;

    public Content(Store<State> store) {
        super(FXML);
        this.store = store;
    }

    private SimpleObjectProperty<Point2D> cursorPosition = new SimpleObjectProperty<>();

    private ContextMenu menu = new ContextMenu();

    @Override
    public void onInit() {
        super.onInit();

        canvas.setManaged(false);

        menu.getItems().addAll(
                MenuItemBuilder.of("Vytvořit blok")
                        .action(() -> store.dispatch(new AddBlock(cursorPosition.get(), new Block())))
                        .build()
        );

        MoveFeature.create()
                .when(App::shouldMovingAlternatively)
                .observeOn(view)
                .applyTo(canvas)
                .install();

        LinkableFeature.create()
                .when(it -> has(it, BlockComp.LINKING))
                .applyTo(view)
                .link(it -> {
                    BlockComp block = comp(it);
                    Port port = new Port(null, block.getModel());
                    store.dispatch(new AddPort(new Point2D(it.getSceneX(), it.getSceneY()), port));

                    PortComp component = store.getState().getActiveScheme().getPorts().get(port);
                    store.dispatch(new LinkExistingAsTarget(block, component));
                })
                .install();

//        ZoomFeature.create().when(App::shouldZoom).observeOn(getView()).applyTo(canvas).install();
    }

    @FXML
    private void onMenuRequested(ContextMenuEvent it) {
        if (store.getState().getActiveScheme() == null) return;
        cursorPosition.set(new Point2D(it.getSceneX(), it.getSceneY()));

        menu.show(view, it.getScreenX(), it.getScreenY());

        it.consume();
    }

    //region Consumers
    public void consume(State state, Action action) {
        if (action instanceof ChangeScheme) changeScheme(state.getActiveScheme());
        else if (action instanceof LinkBlocks) linkBlocks(state.getActiveScheme(), (LinkBlocks) action);
        else if (action instanceof LinkExistingAsSource) linkSource((LinkExistingAsSource) action);
        else if (action instanceof LinkExistingAsTarget) linkTarget((LinkExistingAsTarget) action);
        else if (action instanceof RemoveLink) removeLink((RemoveLink) action);
        else if (action instanceof AddBlock) addBlock(state.getActiveScheme(), (AddBlock) action);
        else if (action instanceof RemoveBlock) removeBlock(state.getActiveScheme(), (RemoveBlock) action);
        else if (action instanceof AddPort) addPort(state.getActiveScheme(), (AddPort) action);
        else if (action instanceof RemovePort) removePort(state.getActiveScheme(), (RemovePort) action);
        else if (action instanceof BlockAction) StateUtils.distribute(state, action);
    }

    private void changeScheme(ActiveScheme scheme) {
        canvas.getChildren().clear();

        scheme.getBlocks().keySet().stream()
                .map(this::blockFactory)
                .forEach(it -> {
                    canvas.getChildren().add(it.getView());
                    translate(it.getView(), random(it.getView(), view.getLayoutBounds()));
                });

        scheme.getPorts().keySet().stream()
                .map(this::portFactory)
                .forEach(it -> {
                    Point2D point;

                    if (it.getSource() != null && it.getTarget() != null)
                        point = midpoint(it.getSource().getBlock().getView(), it.getTarget().getBlock().getView());
                    else if (it.getSource() != null)
                        point = pos(new Point2D(-10, 0), it.getView());
                    else
                        point = pos(new Point2D(10, 0), it.getView());

                    canvas.getChildren().add(it.getView());
                    translate(it.getView(), point);
                });
    }

    private void addBlock(ActiveScheme content, AddBlock action) {
        BlockComp component = blockFactory(action.block);
        content.getBlocks().put(action.block, component);
        canvas.getChildren().add(component.getView());

        translate(component.getView(), action.position);
    }

    private void removeBlock(ActiveScheme content, RemoveBlock action) {
        canvas.getChildren().remove(StateUtils.getBlockComponent(content, action.block).getView());
        content.getBlocks().remove(action.block);
    }

    private void addPort(ActiveScheme content, AddPort action) {
        PortComp component = portFactory(action.port);
        content.getPorts().put(action.port, component);
        canvas.getChildren().add(component.getView());

        translate(component.getView(), action.position);
    }

    private void removePort(ActiveScheme content, RemovePort action) {
        canvas.getChildren().remove(StateUtils.getPortComponent(content, action.port).getView());
        content.getPorts().remove(action.port);
    }

    private void linkBlocks(ActiveScheme content, LinkBlocks action) {
        PortComp component = portFactory(new Port(action.source.getModel(), action.target.getModel()));
        content.getPorts().put(component.getModel(), component);

        SourceLink sourceLink = new SourceLink(store, action.source, component);
        TargetLink targetLink = new TargetLink(store, action.target, component);

        Platform.runLater(() -> { // At once
            canvas.getChildren().add(component.getView());
            canvas.getChildren().addAll(sourceLink.getView(), targetLink.getView());
            translate(component.getView(), midpoint(action.source.getView(), action.target.getView()));
        });
    }

    private void linkTarget(LinkExistingAsTarget action) {
        Link link = new TargetLink(store, action.block, action.port);
        canvas.getChildren().add(link.getView());
    }

    private void linkSource(LinkExistingAsSource action) {
        Link link = new SourceLink(store, action.block, action.port);
        canvas.getChildren().add(link.getView());
    }

    private void removeLink(RemoveLink action) {
        canvas.getChildren().remove(action.link.getView());
    }
    //endregion

    private BlockComp blockFactory(Block block) {
        BlockComp component = new BlockComp(store, block);

        PlatformMoveFeature.create()
                .when(App::shouldMove)
                .observeOn(view)
                .applyTo(component.getView())
                .with(BlockComp.MOVING, UUID.randomUUID().toString())
                .install();

        LinkFeature.create()
                .when(App::shouldLink)
                .applyTo(component.getView())
                .with(BlockComp.LINKING, true)
                .install();

        LinkableFeature.create()
                .when(it -> has(it, BlockComp.LINKING) && notMe(it, component))
                .applyTo(component.getView())
                .link(it -> {
                    BlockComp source = comp(it);
                    Port port = new Port(source.getModel(), component.getModel());
                    store.dispatch(new AddPort(midpoint(source.getView(), component.getView()), port));

                    PortComp through = store.getState().getActiveScheme().getPorts().get(port);
                    LinkAction.betweenBlocks(source, component, through).forEach(store::dispatch);
                })
                .install();

//        LinkableFeature.create()
//                .when(it -> has(it, PortComp.LINKING) && noLinkBetween(component, comp(it)))
//                .applyTo(component.getView())
//                .link(it -> store.dispatch(LinkAction.betweenExisting( component, comp(it))))
//                .install();

        return component;
    }

    private PortComp portFactory(Port port) {
        PortComp component = new PortComp(store, port);

        PlatformMoveFeature.create()
                .when(App::shouldMove)
                .observeOn(view)
                .applyTo(component.getView())
                .with(BlockComp.MOVING, UUID.randomUUID().toString())
                .install();

//        LinkFeature.create()
//                .when(it -> App.shouldLink(it) && canBeLinked(component))
//                .with(PortComp.LINKING, true)
//                .applyTo(component.getView())
//                .install();

//        LinkableFeature.create()
//                .when(it -> has(it, BlockComp.LINKING) && canBeLinked(component) && noLinkBetween(comp(it), component))
//                .applyTo(component.getView())
//                .link(it -> store.dispatch(LinkAction.betweenExisting(comp(it), component)))
//                .install();

        return component;
    }

    private static boolean notMe(DragEvent event, Component me) {
        return event.getGestureSource() != me.getView();
    }

    private static boolean has(DragEvent event, DataFormat format) {
        return event.getDragboard().hasContent(format);
    }
}
