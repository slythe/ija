package ija.calc;

import ija.model.*;
import java.util.*;

/**
 * Class representation of equation.
 *
 * @author Vojtěch Bargl (xbargl01@stud.fit.vutbr.cz)
 */
public interface Equation {
    /** Get used variables that must be passed to {@link Equation::execute()}. */
    List<String> getVariables();

    /** Get expression */
    String getExpression();

    /** Execute equation with given variables and return result.
     * Behavior is not defined when some variables are not passed. */
    Double execute(List<DomainEntry> variables);
}
