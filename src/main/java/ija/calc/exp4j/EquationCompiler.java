package ija.calc.exp4j;

import ija.calc.Equation;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

public class EquationCompiler implements ija.calc.EquationCompiler {
    @Override
    public Equation compile(String expression) {
        Expression compiledExpression = new ExpressionBuilder(expression)
                .variables("x", "y")
                .build();

        return new ija.calc.exp4j.Equation(compiledExpression, expression);
    }
}
