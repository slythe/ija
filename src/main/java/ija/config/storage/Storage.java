package ija.config.storage;

import ija.config.serialization.Serializable;
import ija.model.Scheme;

import java.util.Collection;

/**
 *
 * Storage interface as dependency inversion principle and repository pattern
 *
 * @author Pavel Parma (xparma02)
 */
public interface Storage {
    Collection<Scheme> load();
    void store(Collection<Scheme> schemes);
}
