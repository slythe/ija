package ija.config.storage;

import ija.config.serialization.Serializer;
import ija.model.Scheme;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.Collections;

/**
 * Memory storage ad basic storage implementation
 *
 * @author Pavel Parma (xparma02)
 */
public class FileStorage implements Storage  {

    private Path path;
    private Serializer serializer;

    public FileStorage(Path path, Serializer serializer) {
        this.path = path;
        this.serializer = serializer;
    }

    @Override
    public Collection<Scheme> load() {
        try {
            if(! Files.exists(this.path)) return Collections.emptyList();

            return this.serializer.deserialize(new String(Files.readAllBytes(this.path)));
        } catch (IOException e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    @Override
    public void store(Collection<Scheme> schemes) {
        try {
            if(Files.exists(path)) Files.delete(this.path);

            Files.write(this.path, this.serializer.serialize(schemes).getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
