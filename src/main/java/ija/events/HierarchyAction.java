package ija.events;

import ija.model.Scheme;

public interface HierarchyAction extends Action {
    class AddScheme implements HierarchyAction {
        public final Scheme scheme;

        public AddScheme(Scheme scheme) {
            this.scheme = scheme;
        }
    }

    class RemoveScheme implements HierarchyAction {
        public final Integer index;

        public RemoveScheme(Integer index) {
            this.index = index;
        }
    }

    class RenameScheme implements HierarchyAction {
        public final Integer index;
        public final String name;

        public RenameScheme(Integer index, String name) {
            this.index = index;
            this.name = name;
        }
    }

    class LoadSchemes implements Action {
    }

    class ChangeScheme implements HierarchyAction {
        public final Integer index;
        public final Scheme scheme;

        public ChangeScheme(Integer index, Scheme scheme) {
            this.index = index;
            this.scheme = scheme;
        }
    }

    class SaveScheme implements HierarchyAction {
    }
}
