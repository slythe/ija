package ija.events;

import ija.gui.components.extra.Link;
import ija.gui.components.model.BlockComp;
import ija.gui.components.model.PortComp;
import ija.utils.CollectionUtils;
import java.util.List;
import javafx.geometry.Point2D;

public interface LinkAction extends ContentAction {
    static LinkAction betweenExisting(BlockComp block, PortComp port) {
        if (port.getSource() == null)
            return new LinkExistingAsSource(block, port);
        else if (port.getTarget() == null)
            return new LinkExistingAsTarget(block, port);

        throw new IllegalStateException("Port has not empty slot!");
    }

    static List<LinkAction> betweenBlocks(BlockComp source, BlockComp target, PortComp through) {
        return CollectionUtils.list(
                new LinkExistingAsSource(source, through),
                new LinkExistingAsTarget(target, through)
        );
    }

    class LinkBlocks implements LinkAction {
        public final BlockComp target;
        public final BlockComp source;

        public LinkBlocks(BlockComp source, BlockComp target) {
            this.source = source;
            this.target = target;
        }
    }

    class LinkExistingAsSource implements LinkAction {
        public final BlockComp block;
        public final PortComp port;

        public LinkExistingAsSource(BlockComp block, PortComp port) {
            this.block = block;
            this.port = port;
        }
    }

    class LinkExistingAsTarget implements LinkAction {
        public final BlockComp block;
        public final PortComp port;

        public LinkExistingAsTarget(BlockComp block, PortComp port) {
            this.block = block;
            this.port = port;
        }
    }

    class LinkNewAsSource implements LinkAction {
        public final Point2D position;
        public final BlockComp block;
        public final PortComp port;

        public LinkNewAsSource(Point2D position, BlockComp block, PortComp port) {
            this.position = position;
            this.block = block;
            this.port = port;
        }
    }

    class RemoveLink implements LinkAction {
        public final BlockComp block;
        public final PortComp port;
        public final Link link;

        public RemoveLink(BlockComp block, PortComp port, Link link) {
            this.block = block;
            this.port = port;
            this.link = link;
        }
    }

}
