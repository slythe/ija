package ija.model;

import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Iterator for computation
 *
 * @author Vojtěch Bargl (xbargl01@stud.fit.vutbr.cz)
 */
public class WaveIterator implements Iterator<Set<Block>> {
    private Set<Block> active;
    private Set<Block> next = null;

    public WaveIterator(Set<Block> initial) {
        this.active = initial;
    }

    private void computeNext() {
        if (next != null) return;

        next = active.stream()
                .map(Block::getOutputs)
                .flatMap(Collection::stream)
                .map(Port::getTarget)
                .collect(Collectors.toSet());
    }

    /** Returns true if iterator has next wave, false otherwise. */
    @Override
    public boolean hasNext() {
        computeNext();
        return !active.isEmpty();
    }

    /** Returns set of blocks if iterator has next wave, {@link NoSuchElementException} is thrown otherwise. */
    @Override
    public Set<Block> next() {
        if (active == null) throw new NoSuchElementException();

        active.forEach(Block::execute);
        computeNext();

        Set<Block> blocks = active;
        active = next;
        next = null;

        return blocks;
    }
}
