package ija.model;

/**
 * Class representation of text -> value pair.
 *
 * @author Vojtěch Bargl (xbargl01@stud.fit.vutbr.cz)
 */
public final class DomainEntry {
    private String name;
    private Double value;

    public DomainEntry() {
    }

    public DomainEntry(String name, Double value) {
        this.name = name;
        this.value = value;
    }

    /** Name of entry. */
    public String getName() {
        return name;
    }

    /** Value of entry. */
    public Double getValue() {
        return value;
    }
}
