package ija.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import ija.config.serialization.Serializable;
import ija.utils.CollectionUtils;
import java.util.Collection;
import java.util.Set;

/**
 * Class representation for set of blocks.
 *
 * @author Vojtěch Bargl (xbargl01@stud.fit.vutbr.cz)
 */
public class Scheme implements Serializable {
    private String name;
    @JsonProperty
    private Set<Block> blocks = CollectionUtils.set();

    public Scheme() {
    }

    public Scheme(String name) {
        this.name = name;

    }

    /** Gets blocks. */
    @JsonIgnore
    public Set<Block> getAll() {
        return CollectionUtils.immutableSet(blocks);
    }

    /** Sets blocks. */
    public void setAll(Collection<Block> blocks) {
        this.blocks = CollectionUtils.set(blocks);
    }

    /** Gets text. */
    public String getName() {
        return name;
    }

    /** Sets text. */
    public void setName(String name) {
        this.name = name;
    }
}
